'use strict'

import * as fs from 'fs/promises';
import fse from 'fs-extra';
import path, { resolve } from 'path';
import { get } from 'http';

let srcDir = 'roliki';
let desDir = 'roliki2';
let arr = [
	'/home/efiradmin/scripts/copy-files/roliki/daytips/4.txt',
	'/home/efiradmin/scripts/copy-files/roliki/gorod/1.txt',
	'/home/efiradmin/scripts/copy-files/roliki/tema/2.txt',
	'/home/efiradmin/scripts/copy-files/roliki/tema/3.txt'
];

async function getFiles(dir) {
	const dirents = await fs.readdir(dir, { withFileTypes: true });
	//console.log(dirents)
	const files = await Promise.all(dirents.map(dirent => {
		//console.log(dirent)
		const res = path.resolve(dir, dirent.name);
		if (dirent.isDirectory()) {

			return getFiles(res)
		} else {
			copyFiles(res, desDir, { recursive: true });
			return res
		}
	}))
	let test = Array.prototype.concat(...files)
	//console.log(test)
	return test
}


getFiles(srcDir)
	.then(files => console.log(files))
	.catch(err => console.error(err))
