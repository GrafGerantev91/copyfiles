'use strict'

import * as fs from 'fs/promises';
import path from 'path';

let srcDir = path.resolve('roliki');
let desDir = path.resolve('roliki2');
let srcFiles = [];
let dstFiles = [];

let enumerationDirs = dir => {
	return fs.readdir(dir);

}

enumerationDirs(srcDir)
	.then(result => {
		let arrayOfPromises = result.map(dir => {
			srcFiles.push(`${srcDir}/${dir}`);
			return fs.mkdir(`${desDir}/${dir}`, {
				recursive: true
			})
		})
		return Promise.all(arrayOfPromises);
	})
	.then(() => {
		return enumerationDirs(desDir);
	})
	.then(result => {
		result.map(dir => {
			dir = `${desDir}/${dir}`
			dstFiles.push(dir);
		})
		return dstFiles;
	})
	.then(result => {

		let copyFilesPromise = srcFiles.map((item, index) => {
			return fs.cp(item, result[index], {
				recursive: true
			});
		});
		return Promise.all(copyFilesPromise)
	})
	.then(() => {
		let gettingCopiedFiles = dstFiles.map(elem => {
			return enumerationDirs(elem)
		})
		return Promise.all(gettingCopiedFiles);
	})
	/* 
		.then(result => {
			result.map(elem => {
				elem.map(item => {
					console.log(item)
				})
			})
		}) */

	.catch(result => {
		console.error('Ошибка:' + result);
	})



async function getFiles(dir) {
	const dirents = await fs.readdir(dir, { withFileTypes: true });
	//console.log(dirents)
	const files = await Promise.all(dirents.map(dirent => {
		//console.log(dirent)
		const res = path.resolve(dir, dirent.name);
		if (dirent.isDirectory()) {

			return getFiles(res)
		} else {
			return res
		}
	}))
	let test = Array.prototype.concat(...files)
	//console.log(test)
	return test
}


/* getFiles(desDir)
	.then(files => console.log(files))
	.catch(err => console.error(err)) */