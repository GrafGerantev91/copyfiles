'use strict'
import * as fs from 'fs/promises';
import fse from 'fs-extra';
import path from 'path';

let srcDir = 'roliki';

async function getFiles(dir) {
	// читаем содержимое директории
	const dirents = await fs.readdir(dir, { withFileTypes: true });
	// как и в прошлом примере проходимся по папкам
	// и, при необходимости рекурсивно вызываем функцию
	const files = await Promise.all(dirents.map((dirent) => {
		const res = path.resolve(dir, dirent.name);
		return dirent.isDirectory() ? getFiles(res) : res;
	}));

	// преобразуем массив файлов в одномерный
	return Array.prototype.concat(...files);
}


// тестируем
getFiles(srcDir)
	.then(files => console.log(files))
	.catch(err => console.error(err))

